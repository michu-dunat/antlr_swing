package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
//        globalSymbols = new GlobalSymbols();
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    } 

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected int add(Integer e1, Integer e2) {
		return e1+e2;
	}
	
	protected int subtract(Integer e1, Integer e2) {
		return e1-e2;
	}
	
	protected int multiplie(Integer e1, Integer e2) {
		return e1*e2;
	}
	
	protected int divide(Integer e1, Integer e2) {
		return e1/e2;
	}
	
	protected int power(Integer e1, Integer e2) {
		return (int) Math.pow(e1.doubleValue(), e2.doubleValue());
	}
	
}
